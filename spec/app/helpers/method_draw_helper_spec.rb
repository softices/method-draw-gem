require 'test_helper'

describe MethodDrawHelper do
  it 'must show svg image' do
    uri = 'test'
    method_draw_show(uri).must_equal "<img src='data:image/svg+xml;base64,#{uri}'  />"
  end
end
