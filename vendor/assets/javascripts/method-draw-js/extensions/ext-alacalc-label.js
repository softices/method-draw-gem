/*
 * ext-alacalc-label.js
 *
 * Copyright(c) 2014 Simon Funke
 *
 */

/*
    This is a extension for method-draw to be used in alacalc.
*/

function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

methodDraw.addExtension("Add food label", function() {'use strict';

        var createFoodLabel = function() {
           // The coordinates for the label position
           var x = 0;
           var y = 0;

           var labelDesign = $("#alacalcLabelType").val();
           var RecipeID = $("#alacalcRecipeID").val();            
           var locale = GetURLParameter("locale");

           svg_url = "/api/v1/recipes/"+RecipeID+"/label.svg?design="+labelDesign+"&locale="+locale; 

           // Load label svg
           $.get(svg_url, function(data) {

                // Add the svg element to the canvas
                svgCanvas.appendSvgString(data);

                // Group the imported graphic as they belong together
                svgCanvas.groupSelectedElements();

                // Move elements to their mouse location
                svgCanvas.moveSelectedElements(x, y, false);
           }, 'text');

        }

        var createDocumentElements = function() {
            /* Creates all document elements for the alacalc plugin */

            // Create a pop up dialog where the user inputs
            // label type
            locale = sessionStorage.getItem('locale');
            sessionStorage.clear();
            var select_options = {
              "uk_2013": locale == 'de' ? 'EU-Stil 1' : 'EU style 1',
              "uk_2013_2": locale == 'de' ? 'EU-Stil 2' : 'EU style 2',
              "uk_2013_vertical_2": locale == 'de' ? 'EU-Stil 2 vertikal' : 'EU style 2 vertical',
              "uk_2013_drinks": locale == 'de' ? 'EU-Stil 1 (Getränke)' : 'EU style 1 (drinks)',
              "uk_2013_drinks_2": locale == 'de' ? 'EU-Stil 2 (Getränke)' : 'EU style 2 (drinks)',
              "uk_2013_drinks_vertical_2": locale == 'de' ? 'EU-Stil 2 vertikal (Getränke)' : 'EU style 2 vertical (drinks)',
              "big_8": locale == 'de' ? 'BIG 8' : 'BIG 8',
              "nutrition_facts_vertical": locale == 'de' ? 'US vertikal' : 'US Nutrition facts vertical',
              "nutrition_facts_vertical_2": locale == 'de' ? 'US Stil 2 vertikal' : 'US Nutrition facts vertical style 2',
              "nutrition_facts_vertical_compact": locale == 'de' ? 'US kompakt' : 'US Nutrition facts compact',
              "nutrition_facts_horizontal": locale == 'de' ? 'US horizontal' : 'US Nutrition facts horizontal',
              "nutrition_facts_horizontal_compact": locale == 'de' ? 'US kompakt horizontal' : 'US Nutrition facts horizontal compact'
            }
            var dialog_label = locale == 'de' ? 'Etiketteninformationen eingeben' : 'Enter label information',
                recipe_label = locale == 'de' ? 'Rezept' : 'Recipe',
                food_label   = locale == 'de' ? 'Etikettyp' : 'Food label type',
                dropdown     = '<div id="alacalcLabelPopupDialog" title="' + dialog_label + '">' + '<label for="alacalcRecipeID">' + recipe_label + ':</label>' + '<select id="alacalcRecipeID"></select>' + '<label for="alacalcLabelType">' + food_label + ':</label>' + '<select id="alacalcLabelType">';
            for (var key in select_options) {
              dropdown += '<option value="' + key + '">' + select_options[key] + '</option>'
            }
            $(dropdown).insertAfter("#svg_editor");

              // Populate dropdown list with recipes
              //
              $.getJSON("/api/v1/recipes", function(data) {
                  $.each( data["recipes"], function( key, recipe ) {
                      $('#alacalcRecipeID').append($('<option>', {value:recipe["id"], text:recipe["name"]}));
                  });
              });

              // Create the dialog window
              // (closed by default)
              $('#alacalcLabelPopupDialog').dialog({
                 modal: true,
                 autoOpen: false,
                 buttons: {
                             'Cancel': function() {
                                   $(this).dialog('close');
                             },
                             'Accept': function() {
                                 // Load the food label and add it to
                                 // the svg canvas
                                 createFoodLabel();
                                 $(this).dialog('close');
                             }
                          }
                });

        }

        // Create the document elements
        createDocumentElements();

        return {
            name: "Alacalc label",

            // The extension icon
            svgicons: "extensions/alacalc-label-icon.xml",

            // Multiple buttons can be added in this array
            buttons: [{
                // Must match the icon ID in alacalc-label-icon.xml
                id: "alacalc_label",

                // This indicates that the button will be added to the "mode"
                // button panel on the left side
                type: "mode",

                // Tooltip text
                title: "Add food label",

                // Events
                events: {
                    // The action taken when the button is clicked on.
                    // For "mode" buttons, any other button will
                    // automatically be de-pressed.
                    'click': function() {

                        // Ask the user for the barcode information
                        $('#alacalcLabelPopupDialog').dialog('open');
                    }
                }
            }]

        };
});
